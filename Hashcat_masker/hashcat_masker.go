package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"unicode"
)

type kv struct {
	Key   string
	Value int
}

func getPasswordMask(password string) string {
	mask := ""
	for _, char := range password {
		if unicode.IsLetter(char) {
			if unicode.IsLower(char) {
				mask += "?l"
			} else {
				mask += "?u"
			}
		} else if unicode.IsDigit(char) {
			mask += "?d"
		} else {
			mask += "?s"
		}
	}
	return mask
}

/*
	func getPasswordMask(password string) string {
		mask := ""
		for _, char := range password {
			if unicode.IsDigit(char) {
				mask += "?d"
			} else if unicode.IsLetter(char) {
				if unicode.IsLower(char) {
					mask += "?l"
				} else {
					mask += "?u"
				}
			} else {
				mask += "?s"
			}
		}
		return mask
	}
*/
func getTopThreeMasks(passwords []string) {
	maskFrequency := make(map[string]int)

	for _, password := range passwords {
		mask := getPasswordMask(password)
		maskFrequency[mask]++
	}

	var frequencyList []kv
	for k, v := range maskFrequency {
		frequencyList = append(frequencyList, kv{k, v})
	}

	sort.Slice(frequencyList, func(i, j int) bool {
		return frequencyList[i].Value > frequencyList[j].Value
	})

	for i, kv := range frequencyList {
		if i == 3 {
			break
		}
		fmt.Printf("%s: %d\n", kv.Key, kv.Value)
	}
}

func getPasswordsFromFile(filename string) ([]string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var passwords []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		passwords = append(passwords, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return passwords, nil
}

func main() {

	passwordsFile := "C:\\Users\\Red\\Desktop\\sample.txt"
	passwords, err := getPasswordsFromFile(passwordsFile)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return
	}
	fmt.Printf("Top 3 masks for %d passwords:\n", len(passwords))
	getTopThreeMasks(passwords)
}
